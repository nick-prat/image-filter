#include "cairo.h"
#include <iostream>
#include <gtk/gtk.h>
#include <nlohmann/detail/exceptions.hpp>
#include <span>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <functional>

#include "filter.hh"

#include "imgfilter.hh"

template<auto T>
void member_connect(GtkWidget*, gpointer data) {
    std::invoke(T, reinterpret_cast<imgfilter_state*>(data));
}

template<auto T>
void member_connect_widget(GtkWidget* widget, gpointer data) {
    std::invoke(T, reinterpret_cast<imgfilter_state*>(data), widget);
}

template<auto T>
void member_connect_alloc(GtkWidget* widget, GtkAllocation* alloc, gpointer data) {
    std::invoke(T, reinterpret_cast<imgfilter_state*>(data), widget, alloc);
}

template<auto T>
void member_connect_cairo(GtkWidget* widget, cairo_t* alloc, gpointer data) {
    std::invoke(T, reinterpret_cast<imgfilter_state*>(data), widget, alloc);
}

using filter_store_t = std::unordered_map<std::string, std::reference_wrapper<filter_action const>>;
using g_argument_t = std::tuple<imgfilter_state&, scaled_filter_action&>;

void increase_filter(GtkWidget* widget, gpointer data) {
    auto& [is, fa] = *reinterpret_cast<g_argument_t*>(data);
    fa.inc();
    is.apply_filter();
}

void decrease_filter(GtkWidget* widget, gpointer data) {
    auto& [is, fa] = *reinterpret_cast<g_argument_t*>(data);
    fa.dec();
    is.apply_filter();
}

void build_action_bar(auto builder, g_argument_t& arg) {
    auto action_bar = gtk_builder_get_object(builder, "imger-action-bar");
    auto button_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);

    auto dec_button = gtk_button_new_from_icon_name("list-remove", GTK_ICON_SIZE_SMALL_TOOLBAR);
    g_signal_connect(dec_button, "clicked", G_CALLBACK(decrease_filter), &arg);

    auto inc_button = gtk_button_new_from_icon_name("list-add", GTK_ICON_SIZE_SMALL_TOOLBAR);
    g_signal_connect(inc_button, "clicked", G_CALLBACK(increase_filter), &arg);

    gtk_container_add(GTK_CONTAINER(button_box), dec_button);
    gtk_container_add(GTK_CONTAINER(button_box), inc_button);
    gtk_action_bar_pack_start(GTK_ACTION_BAR(action_bar), button_box);
}

void connect_signals(auto builder, auto& is) {
    auto open_button = gtk_builder_get_object(builder, "imger-open-button");
    g_signal_connect(open_button, "clicked", G_CALLBACK(member_connect_widget<&imgfilter_state::open_image>), &is);

    auto save_button = gtk_builder_get_object(builder, "imger-save-button");
    g_signal_connect(save_button, "clicked", G_CALLBACK(member_connect_widget<&imgfilter_state::save_image>), &is);

    auto img_drawing_area = gtk_builder_get_object(builder, "imger-drawing-area");
    g_signal_connect(img_drawing_area, "draw", G_CALLBACK(member_connect_cairo<&imgfilter_state::resize_image>), &is);
}

auto main(int argc, char** argv) -> int {
	gtk_init(&argc, &argv);

    darken_filter_action dfilter{};
    fast_blur_filter_action fbfilter{};
    identity_filter_action ifilter{};
    box_blur_filter_action bbfilter{5};
    segmentation_filter_action sfilter{};
    black_white_filter_action bwfilter{};
    edge_detection_filter_action edfilter{};
    sharpen_filter_action shfilter{};
    gaussian_blur_filter_action gbfilter{};

    filter_store_t fs{};
    fs.insert({"darken", dfilter});
    fs.insert({"fast_blur", dfilter});
    fs.insert({"identity", ifilter});
    fs.insert({"box_blur", bbfilter});
    fs.insert({"segmentation", sfilter});
    fs.insert({"black_white", bwfilter});
    fs.insert({"edge_detection", edfilter});
    fs.insert({"sharpen", shfilter});
    fs.insert({"gaussian", gbfilter});

    // auto bf = multi_filter{{fs.at("box_blur"), fs.at("darken")}};
    auto bf = multi_filter{{fs.at("gaussian")}};
    imgfilter_state is{&bf};

    auto builder = gtk_builder_new_from_file("imgfilter.glade");
    connect_signals(builder, is);

    // auto arg = g_argument_t{is, dfilter};
    // build_action_bar(builder, arg);

    is.set_drawing_area(GTK_WIDGET(gtk_builder_get_object(builder, "imger-drawing-area")));

    auto window = gtk_builder_get_object(builder, "imger-window");
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), nullptr);

    gtk_widget_show_all(GTK_WIDGET(window));
    gtk_main();

    return 0;
}
