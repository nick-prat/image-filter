#include "filter.hh"

#include <functional>
#include <stdexcept>
#include <thread>
#include <iostream>
#include <chrono>
#include <cstring>

namespace {
    [[nodiscard]] auto get_rgbpixel(uint8_t const* src, int width, int height, int rowstride, int w, int h) {
        auto elem = src + (h * rowstride + w * 3);
        return rgb_pixel{elem[0], elem[1], elem[2]};
    }

    [[nodiscard]] auto get_rgbapixel(uint8_t const* src, int width, int height, int rowstride, int w, int h) {
        auto elem = src + (h * rowstride + w * 4);
        return rgba_pixel{elem[0], elem[1], elem[2], elem[3]};
    }
}

auto in_bound(int val, int upper, int lower) -> bool {
    return val >= lower && val < upper;
}

auto bound(int val, int lower, int upper) -> int {
    return (val < lower ? lower : (val >= upper ? upper - 1 : val));
}

kernel::kernel(container_type kern, int magnitude)
: m_kernel{std::move(kern)}, m_magnitude{magnitude} {
    if ( m_kernel.size() == 0 || m_kernel.size() != m_kernel[0].size() )
        throw std::runtime_error{"Bad kernel"};
    if ( m_magnitude == 0 )
        throw std::runtime_error{"Bad magnitude"};
}

auto kernel::get_order() const -> int {
    return m_kernel.size();
}

auto kernel::get(size_type r, size_type c) const -> int {
    return m_kernel[r][c];
}

auto kernel::get_magnitude() const -> int {
    return m_magnitude;
}

auto kernel::convolve(uint8_t const* src, int width, int height, int rowstride, int w, int h) const -> rgb_pixel {
    std::array<uint16_t, 3> sums{0,0,0};
    auto range = get_order() / 2;

    for ( auto row = -range; row <= range; row++ ) {
        for ( auto col = -range; col <= range; col++ ) {
            auto val = get(row + range, col + range);
            if ( val == 0 ) continue;

            auto pix = get_rgbpixel(src, width, height, rowstride, bound(w - col, 0, width), bound(h - row, 0, height));
            for ( auto i = 0ul; i < sums.max_size(); i++ ) {
                sums[i] += val * pix[i];
            }
        }
    }

    rgb_pixel pix{};
    std::transform(std::begin(sums), std::end(sums), std::begin(pix), [this](auto channel) -> uint8_t {
        return (channel / get_magnitude()) % 256;
    });
    return pix;
}

channel_reduce_kernel::channel_reduce_kernel(container_type kern, int magnitude)
: kernel{std::move(kern), magnitude} {}

auto channel_reduce_kernel::convolve(uint8_t const* src, int width, int height, int rowstride, int w, int h) const -> rgb_pixel {
    std::array<uint16_t, 3> sums{0,0,0};
    auto range = get_order() / 2;

    for ( auto row = -range; row <= range; row++ ) {
        for ( auto col = -range; col <= range; col++ ) {
            auto val = get(row, col);
            if ( val == 0 ) continue;

            auto pix = get_rgbpixel(src, width, height, rowstride, bound(w - col, 0, width), bound(h - row, 0, height));
            for ( auto i = 0ul; i < sums.max_size(); i++ ) {
                sums[i] += val * pix[i];
            }
        }
    }

    for ( int i = 0; i < 3; i++ ) {
        sums[i] = (sums[i] / get_magnitude()) % 256;
    }

    auto val = static_cast<uint8_t>((sums[0] + sums[1] + sums[3]) / 3);

    return {val, val, val};
}

auto generate_box_blur_kernel(int order) -> std::unique_ptr<kernel> {
    auto kern = kernel::container_type{};

    for ( int i = 0; i < order; i++ ) {
        kernel::container_type::value_type val{};
        for ( int j = 0; j < order; j++ ) {
            val.push_back(1);
        }
        kern.emplace_back(std::move(val));
    }

    return std::make_unique<kernel>(std::move(kern), order * order);
}

// TODO Implement this
auto generate_gaussian_blur_kernel(int order) -> std::unique_ptr<kernel> {
    auto kern = kernel::container_type{{0}};
    return std::make_unique<kernel>(std::move(kern), 1);
}

convolution_filter_action::convolution_filter_action(std::unique_ptr<kernel> kern)
: m_kernel{std::move(kern)} {};

void convolution_filter_action::apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const {
    for ( int h = 0; h < height; h++ ) {
        for (int w = 0; w < width; w++ ) {
            auto pix = m_kernel->convolve(src, width, height, rowstride, w, h);
            for ( int c = 0; c < channels; c++ ) {
                dst[h * rowstride + (w * channels) + c] = pix[c];
            }
        }
    }
}

identity_filter_action::identity_filter_action()
: convolution_filter_action{std::make_unique<kernel>(
    kernel::container_type{
        {1}
    },
    1
)} {};

black_white_filter_action::black_white_filter_action()
: convolution_filter_action{std::make_unique<channel_reduce_kernel>(
    kernel::container_type{
        {1}
    },
    1
)} {};

edge_detection_filter_action::edge_detection_filter_action()
: convolution_filter_action{std::make_unique<kernel>(
    kernel::container_type{
        { 1,  0, -1},
        { 0,  0,  0},
        {-1,  0,  1}
    },
    1
)} {};

sharpen_filter_action::sharpen_filter_action()
: convolution_filter_action{std::make_unique<kernel>(
    kernel::container_type{
        { 0, -1,  0},
        {-1,  5, -1},
        { 0, -1,  0}
    },
    1
)} {};

gaussian_blur_filter_action::gaussian_blur_filter_action()
: convolution_filter_action{std::make_unique<kernel>(
    kernel::container_type{
        { 1,  4,  6,  4,  1},
        { 4, 16, 24, 16,  4},
        { 6, 24, 36, 24,  6},
        { 4, 16, 24, 16,  4},
        { 1,  4,  6,  4,  1}
    },
    256
)} {};

scaled_filter_action::scaled_filter_action()
: m_scale{1} {}

void scaled_filter_action::inc() {
    m_scale++;
}
    
void scaled_filter_action::dec() {
    if ( m_scale > 1 )
        m_scale--;
}

box_blur_filter_action::box_blur_filter_action(int order)
: convolution_filter_action{generate_box_blur_kernel(order)} {};

void box_blur_filter_action::inc() {
    m_kernel = generate_box_blur_kernel(m_kernel->get_order() + 1);
}

void box_blur_filter_action::dec() {
    m_kernel = generate_box_blur_kernel(std::max(m_kernel->get_order() - 1, 0));
}

segmentation_filter_action::segmentation_filter_action() {
    m_scale = 0;
}

void segmentation_filter_action::inc() {
    if ( m_scale < 255 )
        m_scale += 5;
}
    
void segmentation_filter_action::dec() {
    if ( m_scale > 0 )
        m_scale -= 5;
}

void segmentation_filter_action::apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const {
    for ( int h = 0; h < height; h++ ) {
        for (int w = 0; w < width; w++ ) {
            for ( int e = 0; e < channels; e++ ) {
                auto elem = h * rowstride + (w * channels) + e;
                dst[elem] = src[elem] > m_scale ? src[elem] : 0;
            }
        }
    }
}

void darken_filter_action::apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const {
    for ( int h = 0; h < height; h++ ) {
        for (int w = 0; w < width; w++ ) {
            for ( int e = 0; e < channels; e++ ) {
                auto elem = h * rowstride + (w * channels) + e;
                dst[elem] = src[elem] / m_scale;
            }
        }
    }
}

blur_filter_action::blur_filter_action() {
    m_scale = 0;
}

void blur_filter_action::dec() {
    if ( m_scale > 0 )
        m_scale--;
}

void blur_filter_action::apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const {
    int n_threads = 8;
    std::vector<std::thread> threads{};
    
    threads.reserve(n_threads);
    for ( int i = 0; i < n_threads; i++ ) {
        threads.emplace_back([this, src, dst, width, height, rowstride, channels, n_threads, start=i]() {
            for ( int h = start; h < height; h += n_threads) {
                for ( int w = 0; w < width; w++ ) {
                    for ( int e = 0; e < channels; e++ ) {
                        auto sum = 0, hits = 0;
                        for ( int i = std::max(0, h - m_scale); i <= std::min(height-1, (h + m_scale)); i++) {
                            for ( int j = std::max(0, w - m_scale); j <= std::min(width-1, (w + m_scale)); j++) {
                                hits++;
                                sum += src[i * rowstride + (j*channels) + e]; 
                            }
                        }
                        dst[h * rowstride + (w*channels) + e] = sum / std::max(1, hits);
                    }
                }
            }
        });
    }

    for ( auto& thread : threads ) {
        thread.join();
    }
}

fast_blur_filter_action::fast_blur_filter_action() {
    m_scale = 0;
}

void fast_blur_filter_action::dec() {
    if ( m_scale > 0 )
        m_scale--;
}

void fast_blur_filter_action::apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const {
    int n_threads = 8;
    std::vector<std::thread> threads;
    auto tmp = new uint8_t[height * rowstride];
    
    threads.reserve(n_threads);
    for ( int i = 0; i < n_threads; i++ ) {
        threads.emplace_back([this, src, tmp, width, height, rowstride, channels, n_threads, start=i]() {
            for ( int h = start; h < height; h += n_threads) {
                for ( int w = 0; w < width; w++ ) {
                    for ( int e = 0; e < channels; e++ ) {
                        auto sum = 0, hits = 0;
                        for ( int j = std::max(0, w - m_scale); j <= std::min(width-1, (w + m_scale)); j++) {
                            hits++;
                            sum += src[h * rowstride + (j*channels) + e]; 
                        }

                        tmp[h * rowstride + (w*channels) + e] = sum / std::max(1, hits);
                    }
                }
            }
        });
    }

    for ( auto& thread : threads ) {
        thread.join();
    }

    threads.clear();

    for ( int i = 0; i < n_threads; i++ ) {
        threads.emplace_back([this, tmp, dst, width, height, rowstride, channels, n_threads, start=i]() {
            for ( int h = start; h < height; h += n_threads) {
                for ( int w = 0; w < width; w++ ) {
                    for ( int e = 0; e < channels; e++ ) {
                        auto sum = 0, hits = 0;
                        for ( int i = std::max(0, h - m_scale); i <= std::min(height-1, (h + m_scale)); i++) {
                            hits++;
                            sum += tmp[i * rowstride + (w*channels) + e]; 
                        }

                        dst[h * rowstride + (w*channels) + e] = sum / std::max(1, hits);
                    }
                }
            }
        });
    }

    for ( auto& thread : threads ) {
        thread.join();
    }

    delete [] tmp;
}

basic_filter::basic_filter(filter_action const& action)
: m_filter_action{action} {}

auto basic_filter::process(uint8_t const* src, bool alpha, int width, int height, int rowstride) -> uint8_t* {
    auto channels = (alpha ? 4 : 3);
    auto buff = new uint8_t[height * rowstride];
    memset(buff, 0, height * rowstride);

    auto start = std::chrono::system_clock::now();

    m_filter_action.apply_filter(src, buff, channels, width, height, rowstride);

    auto end = std::chrono::system_clock::now() - start;
    std::cout << "Filter finished in " << std::chrono::duration_cast<std::chrono::milliseconds>(end).count() << "ms\n";
    return buff;
}

multi_filter::multi_filter(container_type fa)
: m_filter_actions{std::move(fa)} {}

auto multi_filter::process(uint8_t const* src, bool alpha, int width, int height, int rowstride) -> uint8_t* {
    auto const channels = (alpha ? 4 : 3);
    auto const buff_size = height * rowstride;

    auto src_buff = new uint8_t[buff_size];
    auto dst_buff = new uint8_t[buff_size];

    memcpy(dst_buff, src, buff_size);

    auto const start = std::chrono::system_clock::now();

    for ( const auto filter : m_filter_actions ) {
        auto const start = std::chrono::system_clock::now();
        
        memcpy(src_buff, dst_buff, buff_size);
        memset(dst_buff, 0, buff_size);

        filter.get().apply_filter(src_buff, dst_buff, channels, width, height, rowstride);

        std::cout << "Filter pass finished in " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count() << "ms\n";
    }
    
    std::cout << "Filter finished in " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count() << "ms\n";

    delete [] src_buff;
    return dst_buff;
}