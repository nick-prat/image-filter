#pragma once

#include <gtk/gtk.h>

#include <string>

#include "filter.hh"

class imgfilter_state {
public:
    imgfilter_state(filter* filt);

    void set_filter(filter* filt);
    void load_from_file(std::string const& filename);
    void save_to_file(std::string const& filename);

    void unload_view_image();
    void unload_image();

    void set_drawing_area(GtkWidget* widget);
    
    void open_image(GtkWidget* app);
    void save_image(GtkWidget* app);

    void resize_image(GtkWidget* widget, cairo_t* cr);

    void apply_filter();

private:
    GtkWidget* m_drawing_area = nullptr;
    GdkPixbuf* m_pixbuf = nullptr;
    GdkPixbuf* m_view_pixbuf = nullptr;
    filter* m_filter = nullptr;
};