#include <string>

#include "imgfilter.hh"


imgfilter_state::imgfilter_state(filter* filt)
: m_filter{filt} {}

void imgfilter_state::set_filter(filter* filt) {
    m_filter = filt;
}

void imgfilter_state::load_from_file(std::string const& filename) {
    unload_image();

    m_pixbuf = gdk_pixbuf_new_from_file(filename.c_str(), nullptr);
    m_view_pixbuf = gdk_pixbuf_copy(m_pixbuf);
    if ( m_drawing_area != nullptr ) {
        apply_filter();
        gtk_widget_queue_draw(m_drawing_area);
    }
}

// TODO Read more here http://www.manpagez.com/html/gdk-pixbuf/gdk-pixbuf-2.36.0/gdk-pixbuf-File-saving.php#gdk-pixbuf-save
void imgfilter_state::save_to_file(std::string const& filename) {
    GError* error = nullptr;
    gdk_pixbuf_save (m_view_pixbuf, filename.c_str(), "jpeg", &error, "quality", "100", NULL);
    if ( error != nullptr )
        g_clear_error(&error);
}

void imgfilter_state::unload_view_image() {
    if ( m_view_pixbuf != nullptr ) {
        g_object_unref(m_view_pixbuf);
        m_view_pixbuf = nullptr;
    }
}

void imgfilter_state::unload_image() {
    unload_view_image();

    if ( m_pixbuf != nullptr ) {
        g_object_unref(m_pixbuf);
        m_pixbuf = nullptr;
    }
}

void imgfilter_state::set_drawing_area(GtkWidget* widget) {
    m_drawing_area = widget;
}

void imgfilter_state::open_image(GtkWidget* app) {
    auto action = GTK_FILE_CHOOSER_ACTION_OPEN;
    auto parent_window = GTK_WINDOW(gtk_widget_get_toplevel(app));
    auto dialog = gtk_file_chooser_dialog_new("Open File",
                                    parent_window,
                                    action,
                                    "_Cancel",
                                    GTK_RESPONSE_CANCEL,
                                    "_Open",
                                    GTK_RESPONSE_ACCEPT,
                                    nullptr);
    
    auto res = gtk_dialog_run(GTK_DIALOG(dialog));
    if (res == GTK_RESPONSE_ACCEPT) {
        auto filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
        load_from_file(filename);
        g_free(filename);
    }

    gtk_widget_destroy(dialog);
}

void imgfilter_state::save_image(GtkWidget* app) {
    auto action = GTK_FILE_CHOOSER_ACTION_SAVE;
    auto parent_window = GTK_WINDOW(gtk_widget_get_toplevel(app));
    auto dialog = gtk_file_chooser_dialog_new("Save File",
                                    parent_window,
                                    action,
                                    "_Cancel",
                                    GTK_RESPONSE_CANCEL,
                                    "_Save",
                                    GTK_RESPONSE_ACCEPT,
                                    nullptr);

    gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), true);

    auto res = gtk_dialog_run(GTK_DIALOG(dialog));
    if (res == GTK_RESPONSE_ACCEPT) {
        auto filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
        save_to_file(filename);
        g_free(filename);
    }

    gtk_widget_destroy(dialog);
}

void imgfilter_state::resize_image(GtkWidget* widget, cairo_t* cr) {
    if ( m_view_pixbuf != nullptr ) {
        auto const view_width = gtk_widget_get_allocated_width(widget);
        auto const view_height = gtk_widget_get_allocated_height(widget);
        auto const view_aspect_ratio = static_cast<float>(view_width) / view_height;

        auto const img_width = gdk_pixbuf_get_width(m_pixbuf);
        auto const img_height = gdk_pixbuf_get_height(m_pixbuf);
        auto const img_aspect_ratio = static_cast<float>(img_width) / img_height;

        std::remove_const<decltype(img_width)>::type new_image_width = 0;
        std::remove_const<decltype(img_height)>::type new_image_height = 0;

        if ( view_aspect_ratio < img_aspect_ratio ) {
            new_image_width = view_width;
            new_image_height = new_image_width / img_aspect_ratio;
        } else {
            new_image_height = view_height;
            new_image_width = new_image_height * img_aspect_ratio;
        }

        auto const temp = gdk_pixbuf_scale_simple(m_view_pixbuf, new_image_width, new_image_height, GDK_INTERP_BILINEAR);

        auto const x = (view_width - new_image_width) / 2;
        auto const y = (view_height - new_image_height) / 2;
        gdk_cairo_set_source_pixbuf(cr, temp, x, y);
        cairo_paint(cr);
        g_object_unref(temp);
    }
}

void imgfilter_state::apply_filter() {
    if ( m_pixbuf == nullptr )
        return;
        
    auto colorspace = gdk_pixbuf_get_colorspace(m_pixbuf);
    auto alpha = gdk_pixbuf_get_has_alpha(m_pixbuf);
    auto bps = gdk_pixbuf_get_bits_per_sample(m_pixbuf);
    auto width = gdk_pixbuf_get_width(m_pixbuf);
    auto height = gdk_pixbuf_get_height(m_pixbuf);
    auto rowstride = gdk_pixbuf_get_rowstride(m_pixbuf);

    auto g_data = gdk_pixbuf_read_pixel_bytes(m_pixbuf);
    auto data = static_cast<uint8_t const*>(g_bytes_get_data(g_data, nullptr));

    auto buff = m_filter->process(data, alpha, width, height, rowstride);

    unload_view_image();
    m_view_pixbuf = gdk_pixbuf_new_from_data(
        buff, 
        colorspace, alpha, bps,
        width, height, rowstride, 
        [](auto /* pixles */, auto data) {
            delete [] static_cast<uint8_t*>(data);
        }, 
        buff
    );

    gtk_widget_queue_draw(m_drawing_area);
}