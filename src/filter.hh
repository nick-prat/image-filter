#pragma once

#include <cstdint>
#include <vector>
#include <array>
#include <memory>

[[nodiscard]] auto in_bound(int val, int lower, int upper) -> bool;
[[nodiscard]] auto bound(int val, int lower, int upper) -> int;

using rgb_pixel = std::array<uint8_t, 3>;
using rgba_pixel = std::array<uint8_t, 4>;

class kernel {
public:
    using size_type = typename std::vector<int>::size_type;
    using container_type = std::vector<std::vector<int>>;

    kernel() = default;
    kernel(kernel const&) = delete;
    kernel(kernel&&) = default;
    virtual ~kernel() = default;

    kernel(container_type kern, int magnitude);

    auto operator=(kernel const&) -> kernel& = delete;
    auto operator=(kernel&&) -> kernel& = delete;

    [[nodiscard]] auto get_order() const -> int;
    [[nodiscard]] auto get(size_type r, size_type c) const -> int;
    [[nodiscard]] auto get_magnitude() const -> int;
    [[nodiscard]] virtual auto convolve(uint8_t const* src, int width, int height, int rowstride, int w, int h) const -> rgb_pixel;

private:
    container_type m_kernel;
    int m_magnitude;
};

class channel_reduce_kernel : public kernel {
public:
    channel_reduce_kernel() = default;
    channel_reduce_kernel(channel_reduce_kernel const&) = delete;
    channel_reduce_kernel(channel_reduce_kernel&&) = default;
    ~channel_reduce_kernel() override = default;

    channel_reduce_kernel(container_type kern, int magnitude);

    auto operator=(channel_reduce_kernel const&) -> channel_reduce_kernel& = delete;
    auto operator=(channel_reduce_kernel&&) -> channel_reduce_kernel& = delete;

    [[nodiscard]] auto convolve(uint8_t const* src, int width, int height, int rowstride, int w, int h) const -> rgb_pixel override;
};

[[nodiscard]] auto generate_box_blur_kernel(int order) -> std::unique_ptr<kernel>;
[[nodiscard]] auto generate_gaussian_blur_kernel(int order) -> std::unique_ptr<kernel>;

class filter_action {
public:
    filter_action() = default;
    filter_action(filter_action const&) = default;
    filter_action(filter_action&&) = default;
    virtual ~filter_action() = default;

    auto operator=(filter_action&&) -> filter_action& = default;
    auto operator=(filter_action const&) -> filter_action& = default;

    virtual void apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const = 0;
};

class convolution_filter_action : public virtual filter_action {
public:
    convolution_filter_action(std::unique_ptr<kernel> kern);
    void apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const override;

protected:
    std::unique_ptr<kernel> m_kernel;
};

class identity_filter_action : public convolution_filter_action {
public:
    identity_filter_action();
};

class black_white_filter_action : public convolution_filter_action {
public:
    black_white_filter_action();
};

class edge_detection_filter_action : public convolution_filter_action {
public:
    edge_detection_filter_action();
};

class sharpen_filter_action : public convolution_filter_action {
public:
    sharpen_filter_action();
};

class gaussian_blur_filter_action : public convolution_filter_action {
public:
    gaussian_blur_filter_action();
};

class scaled_filter_action : public virtual filter_action {
public:
    scaled_filter_action();
    virtual void inc();
    virtual void dec();

protected:
    int m_scale;
};

class box_blur_filter_action : public convolution_filter_action, public scaled_filter_action {
public:
    box_blur_filter_action(int order);
    void inc() override;
    void dec() override;
};

class segmentation_filter_action : public scaled_filter_action {
public:
    segmentation_filter_action();
    void inc() override;
    void dec() override;
    void apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const  override;
};

class darken_filter_action : public scaled_filter_action {
public:
    void apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const  override;
};

class blur_filter_action : public scaled_filter_action {
public:
    blur_filter_action();
    void dec() override;
    void apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const  override;
};

class fast_blur_filter_action : public scaled_filter_action {
public:
    fast_blur_filter_action();
    void dec() override;
    void apply_filter(uint8_t const* src, uint8_t* dst, int channels, int width, int height, int rowstride) const override;
};

class filter {
public:
    filter() = default;
    filter(filter const&) = default;
    filter(filter&&) = default;
    virtual ~filter() = default;

    auto operator=(filter&&) -> filter& = default;
    auto operator=(filter const&) -> filter& = default;

    [[nodiscard]] virtual auto process(uint8_t const* src, bool alpha, int width, int height, int rowstride) -> uint8_t* = 0;
};

class basic_filter : public filter {
public:
    basic_filter(filter_action const& fa);
    [[nodiscard]] auto process(uint8_t const* src, bool alpha, int width, int height, int rowstride) -> uint8_t* override;

private:
    filter_action const& m_filter_action;
};

class multi_filter : public filter {
public:
    using container_type = std::vector<std::reference_wrapper<filter_action const>>;

    multi_filter(container_type fa);
    [[nodiscard]] auto process(uint8_t const* src, bool alpha, int width, int height, int rowstride) -> uint8_t* override;

private:
    container_type m_filter_actions;
};